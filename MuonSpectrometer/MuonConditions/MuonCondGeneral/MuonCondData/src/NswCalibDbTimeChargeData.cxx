/*
  Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
*/

#include "MuonCondData/NswCalibDbTimeChargeData.h"
#include "MuonIdHelpers/IMuonIdHelperSvc.h"
#include "Identifier/Identifier.h"
#include "GeoModelHelpers/throwExcept.h"

std::ostream& operator<<(std::ostream& ostr, const NswCalibDbTimeChargeData::CalibConstants& obj) {
    ostr<<"slope: "<<std::setprecision(15)<<obj.slope;//<<" pm "<<std::setprecision(15)<<obj.slopeError;
    ostr<<" intercept: "<<std::setprecision(15)<<obj.intercept;//<<" pm "<<std::setprecision(15)<<obj.interceptError;
    return ostr;
}

// general functions ---------------------------------
NswCalibDbTimeChargeData::NswCalibDbTimeChargeData(const Muon::IMuonIdHelperSvc* idHelperSvc):
    AthMessaging{"NswCalibDbTimeChargeData"},
    m_idHelperSvc{idHelperSvc} {
        m_pdo_data.resize(m_nStgcElements + m_nMmElements);
        m_tdo_data.resize(m_nStgcElements + m_nMmElements);
}

int NswCalibDbTimeChargeData::identToModuleIdx(const Identifier& chan_id) const{
    const IdentifierHash hash = m_idHelperSvc->detElementHash(chan_id);
    if (m_idHelperSvc->isMM(chan_id)) {
        const MmIdHelper& idHelper{m_idHelperSvc->mmIdHelper()};
        return 4 * static_cast<int>(hash) + idHelper.gasGap(chan_id) -1;
    }
    const sTgcIdHelper& idHelper{m_idHelperSvc->stgcIdHelper()};
    return m_nMmElements + static_cast<int>(hash)*12 + 
            3* (idHelper.gasGap(chan_id) - 1) + idHelper.channelType(chan_id);          
 }

// setting functions ---------------------------------

// setData
void
NswCalibDbTimeChargeData::setData(CalibDataType type, 
                                  const Identifier& chnlId,  
                                  CalibConstants constants) {
    ChannelCalibMap& calibMap =  type == CalibDataType::PDO ? m_pdo_data : m_tdo_data; 
    
    
    const int array_idx = identToModuleIdx(chnlId);   
    ATH_MSG_VERBOSE("Set "<<(type == CalibDataType::PDO  ? "PDO" : "TDO")<<" calibration constants for channel "
                <<m_idHelperSvc->toString(chnlId)<<", slot: "<< array_idx<<", "<<constants);
    CalibModule& calib_mod = calibMap.at(array_idx);
    const unsigned int channel = (m_idHelperSvc->isMM(chnlId) ? 
                                    m_idHelperSvc->mmIdHelper().channel(chnlId) : 
                                    m_idHelperSvc->stgcIdHelper().channel(chnlId)) -1;
    if (calib_mod.channels.empty()) {
        calib_mod.layer_id = m_idHelperSvc->layerId(chnlId);
    }
    if (calib_mod.channels.size() <= channel) calib_mod.channels.resize(channel +1);
    if (calib_mod.channels[channel]) {
        THROW_EXCEPTION("setData() -- Cannot overwrite channel "<<m_idHelperSvc->toString(chnlId)
                        <<"Layer ID: "<<m_idHelperSvc->toString(calib_mod.layer_id)<<
                        " "<<(*calib_mod.channels[channel] ));
        return;
    }    
    calib_mod.channels[channel] = std::make_unique<CalibConstants>(std::move(constants));    
}

// setZeroData
void
NswCalibDbTimeChargeData::setZero(CalibDataType type, MuonCond::CalibTechType tech,  CalibConstants constants) {
    ZeroCalibMap& calibMap = m_zero[tech];    
    calibMap.insert(std::make_pair(type, std::move(constants)));
}



// retrieval functions -------------------------------

// getChannelIds
std::vector<Identifier>
NswCalibDbTimeChargeData::getChannelIds(const CalibDataType type, const std::string& tech, const std::string& side) const {
    std::vector<Identifier> chnls;
    const ChannelCalibMap& calibMap = type == CalibDataType::PDO ? m_pdo_data : m_tdo_data;    
    chnls.reserve(calibMap.size());
    for (const CalibModule& module : calibMap) {
        /// No calibration constants saved here
        if (module.channels.empty()) continue;
        if (side == "A" && m_idHelperSvc->stationEta(module.layer_id) < 0) continue;
        if (side == "C" && m_idHelperSvc->stationEta(module.layer_id) > 0) continue;

        if (m_idHelperSvc->isMM(module.layer_id)) {
            if (tech == "STGC") continue;
            const MmIdHelper& idHelper{m_idHelperSvc->mmIdHelper()};
            for (unsigned int chn = 1 ; chn <= module.channels.size() ; ++chn) {
                if (!module.channels[chn -1]) continue;
                
                chnls.push_back(idHelper.channelID(module.layer_id, 
                                                   idHelper.multilayer(module.layer_id), 
                                                   idHelper.gasGap(module.layer_id), chn ));
            }
        } else if (m_idHelperSvc->issTgc(module.layer_id)) {
            if (tech == "MM") break;
            const sTgcIdHelper& idHelper{m_idHelperSvc->stgcIdHelper()};
            for (unsigned int chn = 1 ; chn <= module.channels.size() ; ++chn) {
                if (!module.channels[chn -1]) continue;
                chnls.push_back(idHelper.channelID(module.layer_id, 
                                                   idHelper.multilayer(module.layer_id), 
                                                   idHelper.gasGap(module.layer_id),  
                                                   idHelper.channelType(module.layer_id), chn ));
            }
        }    
    }

    return chnls;
}
const NswCalibDbTimeChargeData::CalibConstants* NswCalibDbTimeChargeData::getCalibForChannel(const CalibDataType type, const Identifier& channelId) const {
    const ChannelCalibMap& calibMap =  type == CalibDataType::PDO ? m_pdo_data : m_tdo_data;    
    const int array_idx = identToModuleIdx(channelId);
    const unsigned int channel = (m_idHelperSvc->isMM(channelId) ? 
                                  m_idHelperSvc->mmIdHelper().channel(channelId) : 
                                  m_idHelperSvc->stgcIdHelper().channel(channelId)) -1;
    if (calibMap.at(array_idx).channels.size() > channel && calibMap[array_idx].channels[channel]) {
        return calibMap[array_idx].channels[channel].get();
    }
    // search for data for channel zero
    const MuonCond::CalibTechType tech = (m_idHelperSvc->issTgc(channelId))? MuonCond::CalibTechType::STGC : MuonCond::CalibTechType::MM;
    return getZeroCalibChannel(type, tech);        

}
const NswCalibDbTimeChargeData::CalibConstants* NswCalibDbTimeChargeData::getZeroCalibChannel(const CalibDataType type, const MuonCond::CalibTechType tech) const{   
    std::map<MuonCond::CalibTechType, ZeroCalibMap>::const_iterator itr = m_zero.find(tech);
    if(itr != m_zero.end()) {
        const ZeroCalibMap& zeroMap = itr->second;
        ZeroCalibMap::const_iterator type_itr = zeroMap.find(type);
        if(type_itr != zeroMap.end()) return &type_itr->second;
    }
    return nullptr;
}


